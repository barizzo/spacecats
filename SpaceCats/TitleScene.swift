//
//  TitleScene.swift
//  SpaceCats
//
//  Created by Brian Rizzo on 6/28/15.
//  Copyright (c) 2015 Brian Rizzo. All rights reserved.
//

import SpriteKit
import AVFoundation

class TitleScene: SKScene {

	var pressStartSFX = SKAction()
	var backgroundMusic = AVAudioPlayer()

	override func didMoveToView(view: SKView) {
		/* Setup your scene here */
		let bgAudioUrlPath = NSBundle.mainBundle().pathForResource("StartScreen", ofType: "mp3")
		let bgAudioUrl = NSURL.fileURLWithPath(bgAudioUrlPath!)

		do {
			try backgroundMusic = AVAudioPlayer(contentsOfURL: bgAudioUrl)
			backgroundMusic.numberOfLoops = -1
			backgroundMusic.prepareToPlay()
			backgroundMusic.play()
		} catch {
			print("can't play background music")
		}

		pressStartSFX = SKAction.playSoundFileNamed("PressStart.caf", waitForCompletion: false)

		let background = SKSpriteNode(imageNamed: "splash_1")
		background.size = self.frame.size
		background.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))
		self.addChild(background)
	}

	override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
		/* Called when a touch begins */
		self.runAction(pressStartSFX)
		backgroundMusic.stop()

		let gameplayScene = GameplayScene()
		gameplayScene.size = self.size
		self.view!.presentScene(gameplayScene, transition: SKTransition.doorsOpenVerticalWithDuration(2))
	}

	override func update(currentTime: CFTimeInterval) {
		/* Called before each frame is rendered */
	}

}
