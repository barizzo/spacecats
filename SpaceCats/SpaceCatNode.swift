//
//  SpaceCatNode.swift
//  SpaceCats
//
//  Created by Brian Rizzo on 6/28/15.
//  Copyright (c) 2015 Brian Rizzo. All rights reserved.
//

import SpriteKit

class SpaceCatNode: SKSpriteNode {

	private var tapAction: SKAction

	init(position: CGPoint) {
		let texture = SKTexture(imageNamed: "spacecat_1")

		let textures = [SKTexture(imageNamed: "spacecat_2"),SKTexture(imageNamed: "spacecat_1")]

		// A Swift class must initialize its own (non-inherited) properties before it calls
		// its superclass’s designated initializer
		tapAction = SKAction.animateWithTextures(textures, timePerFrame: 0.25)

		// We must call the Designated Initializer, which is the one below
		super.init(texture: texture, color: SKColor.clearColor(), size: texture.size())

		// After we're init, we can refer to ourself as self
		self.position = position // position is an inherited property
		self.anchorPoint = CGPointMake(0.5, 0)
		self.name = "SpaceCat"
	}

	required init?(coder aDecoder: NSCoder) {
	    fatalError("init(coder:) has not been implemented")
	}

	func performTapAction() {
		self.runAction(tapAction)
	}

}
