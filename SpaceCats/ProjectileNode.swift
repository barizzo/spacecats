//
//  ProjectileNode.swift
//  SpaceCats
//
//  Created by Brian Rizzo on 7/1/15.
//  Copyright (c) 2015 Brian Rizzo. All rights reserved.
//

import SpriteKit

class ProjectileNode: SKSpriteNode {

	init(position: CGPoint) {
		let texture = SKTexture(imageNamed: "projectile_1")

		// We must call the Designated Initializer, which is the one below
		super.init(texture: texture, color: SKColor.clearColor(), size: texture.size())

		// After we're init, we can refer to ourself as self
		self.position = position // position is an inherited property
		self.zPosition = 4
		self.name = "Projectile"

		self.setupAnimation()
		self.setupPhysicsBody()

	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func setupPhysicsBody() {
		self.physicsBody = SKPhysicsBody(rectangleOfSize: self.frame.size)
		self.physicsBody?.affectedByGravity = false
		self.physicsBody?.categoryBitMask = PhysicsCategories.projectile
		self.physicsBody?.collisionBitMask = 0
		self.physicsBody?.contactTestBitMask = PhysicsCategories.enemy
	}

	func setupAnimation() {
		let textures = [
			SKTexture(imageNamed: "projectile_1"),
			SKTexture(imageNamed: "projectile_2"),
			SKTexture(imageNamed: "projectile_3")
		]
		let projectileAnimation = SKAction.animateWithTextures(textures, timePerFrame: 0.1)
		let projectileAnimationRepeat = SKAction.repeatActionForever(projectileAnimation)
		self.runAction(projectileAnimationRepeat)
	}

	func moveTowardsPosition(position: CGPoint) { // position is position touched on screen
		// slope = (Y3 - Y1) / (X3 - X1)
		let slope = (position.y - self.position.y) / (position.x - self.position.x)

		// slope = (Y2 - Y1) / (X2 - X1)
		// Y2 - Y1 = slope(X2- X1)
		// Y2 = slope * X2 - slope * X1 + Y1
		let offscreenX: CGFloat
		if(position.x <= self.position.x) {
			offscreenX = -10
		} else {
			offscreenX = self.parent!.frame.size.width + 10
		}
		let offscreenY: CGFloat = slope * offscreenX - slope * self.position.x + self.position.y

		let pointOffscreen = CGPointMake(offscreenX, offscreenY)

		let distanceA = pointOffscreen.y - self.position.y
		let distanceB = pointOffscreen.x - self.position.x
		let distanceC = sqrt(powf(Float(distanceA), 2) + powf(Float(distanceB), 2))

		// distance = speed * time
		// time = distance / speed
		let time = distanceC / Float(Util.PROJECTILE_SPEED);
		let waitToFade = time * 0.75
		let fadeTime = time - waitToFade

		//let moveProjectile = SKAction.moveTo(pointOffscreen, duration: NSTimeInterval(time))
		//self.runAction(moveProjectile)

		let actionSeq = SKAction.sequence([
			SKAction.waitForDuration(NSTimeInterval(waitToFade)),
			SKAction.fadeOutWithDuration(NSTimeInterval(fadeTime)),
			SKAction.removeFromParent()
		])

		let actionGrp = SKAction.group([
			SKAction.moveTo(pointOffscreen, duration: NSTimeInterval(time)),
			actionSeq
		])

		self.runAction(actionGrp)
	}

}
