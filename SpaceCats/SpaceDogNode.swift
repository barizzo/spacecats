//
//  SpaceDogNode.swift
//  SpaceCats
//
//  Created by Brian Rizzo on 7/2/15.
//  Copyright (c) 2015 Brian Rizzo. All rights reserved.
//

import SpriteKit

class SpaceDogNode: SKSpriteNode {

	enum SpaceDogType {
		case TypeA, TypeB
	}

	init(type:SpaceDogType) {
		if(type == .TypeA) {
			let texture = SKTexture(imageNamed: "spacedog_A_1")
			super.init(texture: texture, color: SKColor.clearColor(), size: texture.size())
			let textures = [
				SKTexture(imageNamed: "spacedog_A_1"),
				SKTexture(imageNamed: "spacedog_A_2"),
				SKTexture(imageNamed: "spacedog_A_3")
			]
			self.runAction(SKAction.repeatActionForever(SKAction.animateWithTextures(textures, timePerFrame: 0.1)))
		} else {
			let texture = SKTexture(imageNamed: "spacedog_B_1")
			super.init(texture: texture, color: SKColor.clearColor(), size: texture.size())

			let textures = [
				SKTexture(imageNamed: "spacedog_B_1"),
				SKTexture(imageNamed: "spacedog_B_2"),
				SKTexture(imageNamed: "spacedog_B_3"),
				SKTexture(imageNamed: "spacedog_B_4")
			]
			self.runAction(SKAction.repeatActionForever(SKAction.animateWithTextures(textures, timePerFrame: 0.1)))
		}

		self.zPosition = 5
		self.setupPhysicsBody()
	}

	required init?(coder aDecoder: NSCoder) {
	    fatalError("init(coder:) has not been implemented")
	}

	func setupPhysicsBody() {
		self.physicsBody = SKPhysicsBody(rectangleOfSize: self.frame.size)
		self.physicsBody?.affectedByGravity = false
		self.physicsBody?.categoryBitMask = PhysicsCategories.enemy
		self.physicsBody?.collisionBitMask = 0
		self.physicsBody?.contactTestBitMask = PhysicsCategories.projectile | PhysicsCategories.ground
	}

}
