//
//  HudNode.swift
//  SpaceCats
//
//  Created by Brian Rizzo on 11/22/15.
//  Copyright © 2015 Brian Rizzo. All rights reserved.
//

import SpriteKit

class HudNode: SKNode {

	var lives = Util.MAX_LIVES
	var score = 0

	init(position: CGPoint, frame:CGRect) {

		// We must call the Designated Initializer, which is the one below
		super.init()

		// After we're init, we can refer to ourself as self
		self.position = position // position is an inherited property
		self.zPosition = 10
		self.name = "HUD"

		let catHead = SKSpriteNode(imageNamed: "HUD_cat_1")
		catHead.position = CGPoint(x:30, y:-10);
		self.addChild(catHead)

		var lastLifeBar = SKSpriteNode?()

		for(var i=0; i < lives; i++) {
			let lifeBar = SKSpriteNode(imageNamed: "HUD_life_1")
			lifeBar.name = "Life \(i+1)"
			self.addChild(lifeBar)
			if(lastLifeBar == nil) {
				lifeBar.position = CGPoint(x: catHead.position.x+30, y: catHead.position.y)
			} else {
				lifeBar.position = CGPoint(x: lastLifeBar!.position.x+10, y: lastLifeBar!.position.y)
			}
			lastLifeBar = lifeBar
		}

		let scoreLabel = SKLabelNode(fontNamed: "Futura-CondensedExtraBold")
		scoreLabel.name = "Score"
		scoreLabel.text = "0"
		scoreLabel.fontSize = 24
		scoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Right
		scoreLabel.position = CGPoint(x: frame.size.width-20, y: -10)
		scoreLabel.zPosition = 11
		self.addChild(scoreLabel)
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

}
