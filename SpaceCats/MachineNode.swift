//
//  MachineNode.swift
//  SpaceCats
//
//  Created by Brian Rizzo on 6/28/15.
//  Copyright (c) 2015 Brian Rizzo. All rights reserved.
//

import SpriteKit

class MachineNode: SKSpriteNode {

	init(position: CGPoint) {
		let texture = SKTexture(imageNamed: "machine_1")

		// We must call the Designated Initializer, which is the one below
		super.init(texture: texture, color: SKColor.clearColor(), size: texture.size())

		// After we're init, we can refer to ourself as self
		self.position = position // position is an inherited property
		self.anchorPoint = CGPointMake(0.5, 0)
		self.name = "Machine"

		let textures = [SKTexture(imageNamed: "machine_1"), SKTexture(imageNamed: "machine_2")]
		let machineAnimation = SKAction.animateWithTextures(textures, timePerFrame: 0.5)
		let machineAnimationRepeat = SKAction.repeatActionForever(machineAnimation)
		self.runAction(machineAnimationRepeat)

	}

	required init?(coder aDecoder: NSCoder) {
	    fatalError("init(coder:) has not been implemented")
	}

}
