//
//  GroundNode.swift
//  SpaceCats
//
//  Created by Brian Rizzo on 7/2/15.
//  Copyright (c) 2015 Brian Rizzo. All rights reserved.
//

import SpriteKit

class GroundNode: SKSpriteNode {

	init(size: CGSize) {
		// We must call the Designated Initializer, which is the one below
		super.init(texture: nil, color: SKColor.clearColor(), size: size)
		self.name = "Ground"
		self.position = CGPointMake(size.width/2, size.height/2)
		self.setupPhysicsBody()
	}

	required init?(coder aDecoder: NSCoder) {
	    fatalError("init(coder:) has not been implemented")
	}

	func setupPhysicsBody() {
		self.physicsBody = SKPhysicsBody(rectangleOfSize: self.frame.size)
		self.physicsBody?.affectedByGravity = false
		self.physicsBody?.dynamic = false
		self.physicsBody?.categoryBitMask = PhysicsCategories.ground
		self.physicsBody?.collisionBitMask = PhysicsCategories.debris
		self.physicsBody?.contactTestBitMask = PhysicsCategories.enemy
	}

}
