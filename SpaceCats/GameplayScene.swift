//
//  GameplayScene.swift
//  SpaceCats
//
//  Created by Brian Rizzo on 6/28/15.
//  Copyright (c) 2015 Brian Rizzo. All rights reserved.
//

import SpriteKit
import AVFoundation

struct Util {
	static let PROJECTILE_SPEED = 400
	static let EARTHS_GRAVITY: CGFloat = -9.8
	static let SPACEDOG_MIN_SPEED = -100
	static let SPACEDOG_MAX_SPEED = -50
	static let MAX_LIVES = 3

	static func randomNumberBetween(min: Int, max: Int) -> Int { // Note that max is non-inclusive
		let rand = Int(arc4random())
		return rand % (max - min) + min
	}
}

struct PhysicsCategories {
	static let enemy: UInt32 = 0x1				// 00000000000000000000000000000001
	static let projectile: UInt32 = 0x1 << 1	// 00000000000000000000000000000010
	static let debris: UInt32 = 0x1 << 2		// 00000000000000000000000000000100
	static let ground: UInt32 = 0x1 << 3		// 00000000000000000000000000001000
}

class GameplayScene: SKScene, SKPhysicsContactDelegate {

	var lastUpdateTimeInterval: NSTimeInterval = 0
	var timeSinceEnemyAdded: NSTimeInterval = 0
	var totalGameplay: NSTimeInterval = 0
	var addEnemyInterval: NSTimeInterval = 1.5
	var minGameSpeed: NSInteger = 0
	var damageSFX = SKAction()
	var explodeSFX = SKAction()
	var laserSFX = SKAction()
	var backgroundMusic = AVAudioPlayer()

	override func didMoveToView(view: SKView) {
		let background = SKSpriteNode(imageNamed: "background_1")
		background.size = self.frame.size
		background.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))
		background.zPosition = 0
		self.addChild(background)

		self.setupAudio()

		let hud = HudNode(position: CGPoint(x: 0, y: self.frame.size.height-20), frame: self.frame)
		self.addChild(hud)

		let machine = MachineNode(position: CGPointMake(CGRectGetMidX(self.frame), 12))
		machine.zPosition = 1;
		self.addChild(machine)

		let spaceCat = SpaceCatNode(position: CGPointMake(machine.position.x, machine.position.y - 1))
		spaceCat.zPosition = 2;
		self.addChild(spaceCat)

		self.physicsWorld.gravity = CGVectorMake(0, Util.EARTHS_GRAVITY)
		self.physicsWorld.contactDelegate = self

		let ground = GroundNode(size: CGSizeMake(self.frame.size.width, 22))
		ground.zPosition = 3
		self.addChild(ground)
	}

	override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
		for touch in (touches) {
			let position = touch.locationInNode(self)
			self.shootProjectileTowardsPosition(position)
		}
	}

	override func update(currentTime: NSTimeInterval) {
		if(lastUpdateTimeInterval > 0) {
			timeSinceEnemyAdded += currentTime - lastUpdateTimeInterval
			totalGameplay += currentTime - lastUpdateTimeInterval
		}
		if(timeSinceEnemyAdded > addEnemyInterval) {
			self.addSpaceDog()
			timeSinceEnemyAdded = 0;
		}
		lastUpdateTimeInterval = currentTime

		if(totalGameplay > 480) {
			addEnemyInterval = 0.5
			minGameSpeed = -175
		} else if(totalGameplay > 240) {
			addEnemyInterval = 0.65
			minGameSpeed = -150
		} else if(totalGameplay > 120) {
			addEnemyInterval = 0.75
			minGameSpeed = -125
		} else if(totalGameplay > 30) {
			addEnemyInterval = 1.0
			minGameSpeed = -100
		}
	}

	func setupAudio() {
		let bgAudioUrlPath = NSBundle.mainBundle().pathForResource("Gameplay", ofType: "mp3")
		let bgAudioUrl = NSURL.fileURLWithPath(bgAudioUrlPath!)

		do {
			try backgroundMusic = AVAudioPlayer(contentsOfURL: bgAudioUrl)
			backgroundMusic.numberOfLoops = -1
			backgroundMusic.prepareToPlay()
			backgroundMusic.play()
		} catch {
			print("can't play background music")
		}

		damageSFX = SKAction.playSoundFileNamed("Damage.caf", waitForCompletion: false)
		explodeSFX = SKAction.playSoundFileNamed("Explode.caf", waitForCompletion: false)
		laserSFX = SKAction.playSoundFileNamed("Laser.caf", waitForCompletion: false)
	}

	func addSpaceDog() {
		let randomSpaceDog = Util.randomNumberBetween(0, max: 2)
		let spaceDog: SpaceDogNode
		if(randomSpaceDog == 0) {
			spaceDog = SpaceDogNode(type: .TypeA)
		} else {
			spaceDog = SpaceDogNode(type: .TypeB)
		}

		let dy = Util.randomNumberBetween(Util.SPACEDOG_MIN_SPEED, max: Util.SPACEDOG_MAX_SPEED)
		spaceDog.physicsBody?.velocity = CGVectorMake(0, CGFloat(dy))

		let scale = CGFloat(Util.randomNumberBetween(85, max: 100)) / CGFloat(100)
		spaceDog.xScale = scale
		spaceDog.yScale = scale

		let y = self.frame.size.height + spaceDog.size.height
		let x = Util.randomNumberBetween(Int(10 + spaceDog.size.width), max: Int(self.frame.width-spaceDog.size.width-10))
		spaceDog.position = CGPointMake(CGFloat(x), CGFloat(y))
		self.addChild(spaceDog)
	}

	func shootProjectileTowardsPosition (position: CGPoint) {
		self.runAction(laserSFX)

		let spaceCat = self.childNodeWithName("SpaceCat") as! SpaceCatNode
		spaceCat.performTapAction()

		let machine = self.childNodeWithName("Machine") as! MachineNode

		let projectile = ProjectileNode(position: CGPointMake(machine.position.x, machine.position.y + machine.frame.size.height - 15))
		self.addChild(projectile)
		projectile.moveTowardsPosition(position)
	}

	func createDebris(position:CGPoint) {
		let numberOfPieces = Util.randomNumberBetween(5,max:20)
		for _ in 0...numberOfPieces {
			let randomPiece = Util.randomNumberBetween(1,max:4)
			let imageName = "debri_\(randomPiece)"

			let debris = SKSpriteNode(imageNamed: imageName)
			debris.position = position
			debris.zPosition = 5
			debris.name = "Debris"

			self.addChild(debris)

			debris.physicsBody = SKPhysicsBody(rectangleOfSize: debris.frame.size)
			debris.physicsBody?.categoryBitMask = PhysicsCategories.debris
			debris.physicsBody?.contactTestBitMask = 0
			debris.physicsBody?.collisionBitMask = PhysicsCategories.ground | PhysicsCategories.debris
			debris.physicsBody?.velocity = CGVectorMake(
				CGFloat(Util.randomNumberBetween(-150, max: 150)),
				CGFloat(Util.randomNumberBetween(150, max: 350))
			)
			debris.runAction(SKAction.waitForDuration(2.0), completion:{debris.removeFromParent()})
		}

		if let explosion = SKEmitterNode(fileNamed: "Explosion.sks") {
			explosion.position = position
			explosion.zPosition = 6;
			self.addChild(explosion)
			explosion.runAction(SKAction.waitForDuration(2.0), completion: {
				explosion.removeFromParent()
			})
		}
	}

	func didBeginContact(contact: SKPhysicsContact) {
		let firstBody, secondBody: SKPhysicsBody
		if(contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask) {
			firstBody = contact.bodyA // enemy
			secondBody = contact.bodyB // projectile
		} else {
			firstBody = contact.bodyB
			secondBody = contact.bodyA
		}

		if(firstBody.categoryBitMask == PhysicsCategories.enemy && secondBody.categoryBitMask == PhysicsCategories.projectile) {
			let spaceDog = firstBody.node as? SpaceDogNode
			let projectile = secondBody.node as? ProjectileNode // fatal error: unexpectedly found nil while unwrapping an Optional value

			self.runAction(explodeSFX)

			spaceDog?.removeFromParent()
			projectile?.removeFromParent()

		}
		if(firstBody.categoryBitMask == PhysicsCategories.enemy && secondBody.categoryBitMask == PhysicsCategories.ground) {
			self.runAction(damageSFX)

			let spaceDog = firstBody.node as? SpaceDogNode
			spaceDog?.removeFromParent()
		}

		self.createDebris(contact.contactPoint)
	}

}
